import {configureStore} from '@reduxjs/toolkit';
import { getAllProductsReducer, getProductReducer, registerProductReducer, verifyProductReducer } from './Reducers/Product';

const store = configureStore({
    reducer: {
        registerProduct: registerProductReducer,
        verifyProduct: verifyProductReducer,
        getProduct: getProductReducer,
        getAllProducts: getAllProductsReducer,
    },
});

export default store;