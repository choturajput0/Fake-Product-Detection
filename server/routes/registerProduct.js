const express = require('express');
const { registerProduct } = require('../controllers/product.js');
const router = express.Router();

router.route('/registerProduct').post(registerProduct);

module.exports = router;