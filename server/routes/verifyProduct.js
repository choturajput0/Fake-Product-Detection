const express = require('express');
const {verifyQRCode, getProductById, getAllProducts} = require('../controllers/verifyProduct.js');
const router = express.Router();

router.route('/verifyProduct').post(verifyQRCode);
router.route('/viewProduct/:id').get(getProductById);
router.route('/allProducts').get(getAllProducts);

module.exports = router;