const mongoose = require('mongoose');

const RetailerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    products: {
        type : Array,
    },
});


const Retailer = mongoose.model('Retailer', RetailerSchema);

module.exports = Retailer;