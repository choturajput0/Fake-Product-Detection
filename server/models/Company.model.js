const mongoose = require('mongoose');

const companySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    products: {
        type : Array,
        default: []
    },
});


const Company = mongoose.model('Company', companySchema);

module.exports = Company;