const Product = require('../models/Product.js');

exports.registerProduct = async (req, res) => {
    try {
        const {
            name,
            price,
            description
        } = req.body;
        const newProduct = new Product({
            name,
            price,
            description
        });
        await newProduct.save();
        res.status(200).json({
            success: true,
            product: newProduct,
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};