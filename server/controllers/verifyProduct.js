const Product = require('../models/Product.js');

exports.verifyQRCode = async (req, res) => {
    try {
        const {qrCode} = req.body;
        let product = await Product.findOne({ _id: qrCode });
        if(!product) {
            return res.status(400).json({
                success: false,
                message: "Product does not exists"
            });
        }
        res.status(200).json({
            success: true,
            message: "Product Exists",
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};

exports.getProductById = async (req, res) => {
    try {
        const product = await Product.findById(req.params.id);
        if(!product) {
            res.status(404).json({
                success: false,
                message: "Product not exists",
            });
        }
        res.status(200).json({
            success: true,
            product,
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};

exports.getAllProducts = async (req, res) => {
    try {
        const products = await Product.find({});
        
        res.status(200).json({
            success: true,
            products,
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};